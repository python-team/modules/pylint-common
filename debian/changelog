pylint-common (0.2.5-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Thu, 02 Jun 2022 21:27:11 -0400

pylint-common (0.2.5-4) unstable; urgency=medium

  * Team upload.
  * Also move testsuite away from pylint3

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 10 Dec 2019 15:17:07 +0100

pylint-common (0.2.5-3) unstable; urgency=medium

  * Team upload.
  * Switch to pylint from pylint3, because the latter is a cruft package
    (Closes: #944825)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 10 Dec 2019 14:59:58 +0100

pylint-common (0.2.5-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org.
  * d/copyright: Use https protocol in Format field.
  * d/control: Remove ancient X-Python-Version field.
  * d/control: Remove ancient X-Python3-Version field.
  * Convert git repository from git-dpm to gbp layout.

  [ Chris Lamb ]
  * Respect nocheck in DEB_BUILD_OPTIONS.

  [ Mattia Rizzolo ]
  * Bump debhelper compat level to 11.
  * Bump Standards-Version to 4.2.1.
    + Set Rules-Requires-Root:no.
  * Drop the python-pylint-common (Python 2) package.
    Pylint doesn't support Py2 anymore.
  * Temporary disable the tests, failing with the new Pylint. Addresses: #884220

 -- Mattia Rizzolo <mattia@debian.org>  Wed, 26 Sep 2018 13:49:21 +0200

pylint-common (0.2.5-1) unstable; urgency=medium

  * New upstream release.
  * Merge to unstable.
  * Bump Standards-Version to 4.0.0.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Tue, 20 Jun 2017 23:09:28 +0800

pylint-common (0.2.2-3~exp1) experimental; urgency=medium

  * Change Uploaders to ChangZhuo Chen (Closes: #863366).
  * Bump compat to 10.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Tue, 13 Jun 2017 18:35:27 +0800

pylint-common (0.2.2-2) unstable; urgency=medium

  * deb/control:
    + update my email address.
    + bump standards version to 3.9.8 (no changes needed).
    + fixed VCS URL (https) [Ondřej Nový].
  * deb/copyright:
    + update my email address, expanded copyright span.

 -- Daniel Stender <stender@debian.org>  Fri, 20 Jan 2017 18:23:38 +0100

pylint-common (0.2.2-1) unstable; urgency=medium

  * New upstream release (Closes: #810231).
  * deb/control:
    + put myself in Uploaders and the group in Maintainer (group uploads
      w/o confirmation are o.k.).
    + add pylint3 to build-depends.
    + updated (astroid no needed) and rearranged build-deps.
  * deb/copyright:
    + updated, changed Upstream-Contact to email in setup.py.
  * deb/rules:
    + enable tests with Python3.
  * deb/watch:
    + used Github release instead of Pypi tarball (tests missing input).
  * Added DEP-8 tests.

 -- Daniel Stender <debian@danielstender.com>  Mon, 11 Jan 2016 20:35:28 +0100

pylint-common (0.2.1-1) unstable; urgency=medium

  * Initial release (Closes: #788237).

 -- Daniel Stender <debian@danielstender.com>  Sun, 26 Jul 2015 00:12:54 +0200
